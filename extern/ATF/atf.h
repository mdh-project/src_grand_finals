//
//  atf.h
//  new_atf_lib
//
//  Created by   on 28/10/2016.
//  Copyright © 2016  . All rights reserved.
//

#ifndef atf_h
#define atf_h

#include "include/abort_conditions.hpp"
#include "include/range.hpp"
#include "include/tp.hpp"
#include "include/coordinate_space.hpp"

#include "include/auc_bandit.hpp"
#include "include/differential_evolution.hpp"
#include "include/exhaustive.hpp"
#include "include/particle_swarm.hpp"
#include "include/pattern_search.hpp"
#include "include/random_search.hpp"
#include "include/round_robin.hpp"
#include "include/annealing.hpp"
#include "include/annealing_tree.hpp"
#include "include/simulated_annealing_opentuner.hpp"
#include "include/torczon.hpp"

#include "include/operators.hpp"
#include "include/predicates.hpp"

#include "include/ocl_wrapper.hpp"
#include "include/ocl_md_hom_wrapper.hpp"
#include "include/cpp_cf.hpp"


#endif /* atf_h */
