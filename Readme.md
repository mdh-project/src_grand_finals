# md_poly: A Performance-Portable Polyhedral Compiler Based on Multi-Dimensional Homomorphisms

This artifact contains the workflow to reproduce the results shown in the paper *md_poly: A Performance-Portable Polyhedral Compiler Based on Multi-Dimensional Homomorphisms* accepted for publication at the [2020 ACM SRC competition](https://cgo-conference.github.io/cgo2020/src/). The user is invited to perform the steps described below.

## Software Requirements

- an OpenCL 1.2 driver and runtime environment
- CMake 2.8.11 or higher
- a compiler supporting C++14 or higher

## Workflow

1. Clone the artifact files:

   `$ git clone https://gitlab.com/mdh-project/src_grand_finals.git`
   
2. Change into the artifact directory:

   `$ cd src_grand_finals`
   
3. Run the experiments:

   `$ ./run_experiments.sh`
   
   **Note:** In case you have multiple OpenCL platforms and devices installed, you can edit the variables `CPU_OCL_PLATFORM` and `CPU_OCL_DEVICE` in line 14 and 15 of `run_experiments.sh`, as well as `GPU_OCL_PLATFORM` and `GPU_OCL_DEVICE` in line 51 and 52.