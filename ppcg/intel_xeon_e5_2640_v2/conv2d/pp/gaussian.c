int main() {
    #define M_VAL 4092
    #define N_VAL 4092

    static float in[M_VAL + 4][N_VAL + 4];
    static float out[M_VAL][N_VAL];

    for (int i = 0; i < (M_VAL + 4) * (N_VAL + 4); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL; ++i) ((float *)out)[i] = 0;

#pragma scop
    for (int i = 0; i < M_VAL; i++) {
        for (int j = 0; j < N_VAL; j++) {
            out[i][j] =
                    (2.0f * in[(2 + i - 2)][(2 + j - 2)] +  4.0f * in[(2 + i - 2)][(2 + j - 1)] +  5.0f * in[(2 + i - 2)][(2 + j - 0)] +  4.0f * in[(2 + i - 2)][(2 + j + 1)] + 2.0f * in[(2 + i - 2)][(2 + j + 2)] +
                     4.0f * in[(2 + i - 1)][(2 + j - 2)] +  9.0f * in[(2 + i - 1)][(2 + j - 1)] + 12.0f * in[(2 + i - 1)][(2 + j - 0)] +  9.0f * in[(2 + i - 1)][(2 + j + 1)] + 4.0f * in[(2 + i - 1)][(2 + j + 2)] +
                     5.0f * in[(2 + i - 0)][(2 + j - 2)] + 12.0f * in[(2 + i - 0)][(2 + j - 1)] + 15.0f * in[(2 + i - 0)][(2 + j - 0)] + 12.0f * in[(2 + i - 0)][(2 + j + 1)] + 5.0f * in[(2 + i - 0)][(2 + j + 2)] +
                     4.0f * in[(2 + i + 1)][(2 + j - 2)] +  9.0f * in[(2 + i + 1)][(2 + j - 1)] + 12.0f * in[(2 + i + 1)][(2 + j - 0)] +  9.0f * in[(2 + i + 1)][(2 + j + 1)] + 4.0f * in[(2 + i + 1)][(2 + j + 2)] +
                     2.0f * in[(2 + i + 2)][(2 + j - 2)] +  4.0f * in[(2 + i + 2)][(2 + j - 1)] +  5.0f * in[(2 + i + 2)][(2 + j - 0)] +  4.0f * in[(2 + i + 2)][(2 + j + 1)] + 2.0f * in[(2 + i + 2)][(2 + j + 2)]) / 159.0f;
        }
    }
#pragma endscop

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}