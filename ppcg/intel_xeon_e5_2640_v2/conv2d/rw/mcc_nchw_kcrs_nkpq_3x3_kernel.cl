typedef struct {
    float values[3][3];
} WEIGHTS;
__kernel void kernel0(__global WEIGHTS *filter, __global float *in, __global float *out)
{
    int b0 = get_group_id(0), b1 = get_group_id(1);
    int t0 = get_local_id(0), t1 = get_local_id(1), t2 = get_local_id(2);
    __local float shared_in[1][1][3][7];
    float private_out[1][1][1][5];

    for (int c0 = 0; c0 <= 511; c0 += 1) {
      private_out[0][0][0][0] = out[((0 * 512 + c0) * 5 + b1) * 5 + 0];
      private_out[0][0][0][1] = out[((0 * 512 + c0) * 5 + b1) * 5 + 1];
      private_out[0][0][0][2] = out[((0 * 512 + c0) * 5 + b1) * 5 + 2];
      private_out[0][0][0][3] = out[((0 * 512 + c0) * 5 + b1) * 5 + 3];
      private_out[0][0][0][4] = out[((0 * 512 + c0) * 5 + b1) * 5 + 4];
      for (int c3 = 0; c3 <= 511; c3 += 1) {
        for (int c6 = 0; c6 <= 2; c6 += 1)
          for (int c7 = 0; c7 <= 6; c7 += 1)
            shared_in[0][0][c6][c7] = in[((0 * 512 + c3) * 7 + (b1 + c6)) * 7 + c7];
        barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
        for (int c6 = 0; c6 <= 4; c6 += 1)
          for (int c8 = -1; c8 <= 1; c8 += 1)
            for (int c9 = -1; c9 <= 1; c9 += 1)
              private_out[0][0][0][c6] += (filter[c0 * 512 + c3].values[c8 + 1][c9 + 1] * shared_in[0][0][c8 + 1][c6 + c9 + 1]);
        barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      }
      out[((0 * 512 + c0) * 5 + b1) * 5 + 0] = private_out[0][0][0][0];
      out[((0 * 512 + c0) * 5 + b1) * 5 + 1] = private_out[0][0][0][1];
      out[((0 * 512 + c0) * 5 + b1) * 5 + 2] = private_out[0][0][0][2];
      out[((0 * 512 + c0) * 5 + b1) * 5 + 3] = private_out[0][0][0][3];
      out[((0 * 512 + c0) * 5 + b1) * 5 + 4] = private_out[0][0][0][4];
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
    }
}
