#!/usr/bin/env bash

nvcc -O3 -gencode=arch=compute_70,code=sm_70 mcc_nchw_kcrs_nkpq_3x3_host.cu mcc_nchw_kcrs_nkpq_3x3_kernel.cu && \

echo "measuring runtime..." && \
./a.out $1 $2 && \
echo "ns"