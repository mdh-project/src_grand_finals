#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include "mcc_nchw_kcrs_nkpq_3x3_kernel.hu"
int main() {
    #define N_VAL 1
    #define K_VAL 512
    #define P_VAL 5
    #define Q_VAL 5
    #define C_VAL 512

    static float in[N_VAL][C_VAL][P_VAL + 2][Q_VAL + 2];
    static WEIGHTS filter[K_VAL][C_VAL];
    static float out[N_VAL][K_VAL][P_VAL][Q_VAL];

    for (int i = 0; i < N_VAL * C_VAL * (P_VAL + 2) * (Q_VAL + 2); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * C_VAL * 3 * 3; ++i) ((float *)filter)[i] = (i % 10) + 1;
    for (int i = 0; i < N_VAL * K_VAL * P_VAL * Q_VAL; ++i) ((float *)out)[i] = 0;

    {
#define cudaCheckReturn(ret) \
  do { \
    cudaError_t cudaCheckReturn_e = (ret); \
    if (cudaCheckReturn_e != cudaSuccess) { \
      fprintf(stderr, "CUDA error: %s\n", cudaGetErrorString(cudaCheckReturn_e)); \
      fflush(stderr); \
    } \
    assert(cudaCheckReturn_e == cudaSuccess); \
  } while(0)
#define cudaCheckKernel() \
  do { \
    cudaCheckReturn(cudaGetLastError()); \
  } while(0)

      WEIGHTS *dev_filter;
      float *dev_in;
      float *dev_out;
      
      cudaCheckReturn(cudaMalloc((void **) &dev_filter, (512) * (512) * sizeof(WEIGHTS)));
      cudaCheckReturn(cudaMalloc((void **) &dev_in, (1) * (512) * (7) * (7) * sizeof(float)));
      cudaCheckReturn(cudaMalloc((void **) &dev_out, (1) * (512) * (5) * (5) * sizeof(float)));
      
      cudaCheckReturn(cudaMemcpy(dev_filter, filter, (512) * (512) * sizeof(WEIGHTS), cudaMemcpyHostToDevice));
      cudaCheckReturn(cudaMemcpy(dev_in, in, (1) * (512) * (7) * (7) * sizeof(float), cudaMemcpyHostToDevice));
      cudaCheckReturn(cudaMemcpy(dev_out, out, (1) * (512) * (5) * (5) * sizeof(float), cudaMemcpyHostToDevice));
      {
        dim3 k0_dimBlock(5, 4, 16);
        dim3 k0_dimGrid(2, 8);
        unsigned long min_runtime = ULONG_MAX;
for (int i = 0; i < 10; ++i) {
    cudaCheckReturn(cudaMemcpy(dev_filter, filter, (512) * (512) * sizeof(WEIGHTS), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_in, in, (1) * (512) * (7) * (7) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_out, out, (1) * (512) * (5) * (5) * sizeof(float), cudaMemcpyHostToDevice));
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_filter, dev_in, dev_out);
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;
}
min_runtime = ULONG_MAX;
for (int i = 0; i < 200; ++i) {
    cudaCheckReturn(cudaMemcpy(dev_filter, filter, (512) * (512) * sizeof(WEIGHTS), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_in, in, (1) * (512) * (7) * (7) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_out, out, (1) * (512) * (5) * (5) * sizeof(float), cudaMemcpyHostToDevice));
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_filter, dev_in, dev_out);
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;
}
printf("%lu", min_runtime);
        cudaCheckKernel();
      }
      
      cudaCheckReturn(cudaMemcpy(out, dev_out, (1) * (512) * (5) * (5) * sizeof(float), cudaMemcpyDeviceToHost));
      cudaCheckReturn(cudaFree(dev_filter));
      cudaCheckReturn(cudaFree(dev_in));
      cudaCheckReturn(cudaFree(dev_out));
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
