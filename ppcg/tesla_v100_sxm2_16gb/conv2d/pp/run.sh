#!/usr/bin/env bash

nvcc -O3 -gencode=arch=compute_70,code=sm_70 gaussian_host.cu gaussian_kernel.cu && \

echo "measuring runtime..." && \
./a.out $1 $2 && \
echo "ns"