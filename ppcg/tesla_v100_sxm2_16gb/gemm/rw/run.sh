#!/usr/bin/env bash

nvcc -O3 -gencode=arch=compute_70,code=sm_70 sgemm_row_major_nn_host.cu sgemm_row_major_nn_kernel.cu && \

echo "measuring runtime..." && \
./a.out $1 $2 && \
echo "ns"