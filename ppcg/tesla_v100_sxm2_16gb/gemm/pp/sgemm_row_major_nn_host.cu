#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include "sgemm_row_major_nn_kernel.hu"
int main() {
    #define M_VAL 1024
    #define N_VAL 1024
    #define K_VAL 1024

    static float A[M_VAL][K_VAL];
    static float B[K_VAL][N_VAL];
    static float C[M_VAL][N_VAL];

    for (int i = 0; i < M_VAL * K_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * N_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL; ++i) ((float *)C)[i] = 0;

    {
#define cudaCheckReturn(ret) \
  do { \
    cudaError_t cudaCheckReturn_e = (ret); \
    if (cudaCheckReturn_e != cudaSuccess) { \
      fprintf(stderr, "CUDA error: %s\n", cudaGetErrorString(cudaCheckReturn_e)); \
      fflush(stderr); \
    } \
    assert(cudaCheckReturn_e == cudaSuccess); \
  } while(0)
#define cudaCheckKernel() \
  do { \
    cudaCheckReturn(cudaGetLastError()); \
  } while(0)

      float *dev_A;
      float *dev_B;
      float *dev_C;
      
      cudaCheckReturn(cudaMalloc((void **) &dev_A, (1024) * (1024) * sizeof(float)));
      cudaCheckReturn(cudaMalloc((void **) &dev_B, (1024) * (1024) * sizeof(float)));
      cudaCheckReturn(cudaMalloc((void **) &dev_C, (1024) * (1024) * sizeof(float)));
      
      cudaCheckReturn(cudaMemcpy(dev_A, A, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
      cudaCheckReturn(cudaMemcpy(dev_B, B, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
      cudaCheckReturn(cudaMemcpy(dev_C, C, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
      {
        dim3 k0_dimBlock(512, 1);
        dim3 k0_dimGrid(2, 74);
        unsigned long min_runtime = ULONG_MAX;
for (int i = 0; i < 10; ++i) {
    cudaCheckReturn(cudaMemcpy(dev_A, A, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_B, B, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_C, C, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_A, dev_B, dev_C);
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;
}
min_runtime = ULONG_MAX;
for (int i = 0; i < 200; ++i) {
    cudaCheckReturn(cudaMemcpy(dev_A, A, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_B, B, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_C, C, (1024) * (1024) * sizeof(float), cudaMemcpyHostToDevice));
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_A, dev_B, dev_C);
    cudaCheckKernel();
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    unsigned long runtime = milliseconds * 1000000;
    if (runtime < min_runtime) min_runtime = runtime;
}
printf("%lu", min_runtime);
        cudaCheckKernel();
      }
      
      cudaCheckReturn(cudaMemcpy(C, dev_C, (1024) * (1024) * sizeof(float), cudaMemcpyDeviceToHost));
      cudaCheckReturn(cudaFree(dev_A));
      cudaCheckReturn(cudaFree(dev_B));
      cudaCheckReturn(cudaFree(dev_C));
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
